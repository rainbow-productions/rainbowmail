// api/websocket.js

const WebSocket = require('ws');
const wss = new WebSocket.Server({ port: 8080 });

const posts = [];

wss.on('connection', ws => {

  // Send existing posts to new clients
  ws.send(JSON.stringify(posts));

  ws.on('message', data => {
    
    // Receive new posts
    const post = JSON.parse(data);
    posts.push(post);
    
    // Broadcast new post
    wss.clients.forEach(client => {
      client.send(JSON.stringify(post));  
    });
    
  });

});
