// client.js

const socket = new WebSocket('ws://localhost:8080');

// Listen for new posts
socket.addEventListener('message', e => {
  const post = JSON.parse(e.data);
  displayPost(post); 
});

// Send a new post
function sendPost(post) {
  socket.send(JSON.stringify(post));
}
