// script.js

const socket = new WebSocket('ws://localhost:8080');

// Add input for name
const nameInput = document.getElementById('name'); 

// Display post with name  
function displayPost(post) {
  const li = document.createElement('li');
  li.innerText = `${post.name}: ${post.text}`;
  feed.appendChild(li);
}

// Listen for form submit
form.addEventListener('submit', e => {

  // Get name and text values
  const name = nameInput.value;
  const text = textarea.value;

  // Create post object
  const post = {
    name,
    text,
    timestamp: Date.now()
  };
  
  // Send post to server
  sendPost(post);
  
  // Clear fields
  nameInput.value = '';
  textarea.value = '';

});

// Handle new posts from server
socket.addEventListener('message', e => {
  
  // Get post from server
  const post = JSON.parse(e.data); 
  
  // Display post with name
  displayPost(post);

});
